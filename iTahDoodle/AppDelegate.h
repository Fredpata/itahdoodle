//
//  AppDelegate.h
//  iTahDoodle
//
//  Created by Bo Yan on 4/6/15.
//  Copyright (c) 2015 Fred Yan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic)UITableView *taskTable;
@property (nonatomic)UITextField *taskField;
@property (nonatomic)UIButton *insertButton;

@property (nonatomic)NSMutableArray *tasks;

-(void)addTasks:(id)sender;

@end

