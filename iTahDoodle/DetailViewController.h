//
//  DetailViewController.h
//  iTahDoodle
//
//  Created by Bo Yan on 4/6/15.
//  Copyright (c) 2015 Fred Yan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

